//
//  FirstViewController.swift
//  ToDoList
//
//  Created by yash yadav on 3/17/16.
//  Copyright © 2016 yash yadav. All rights reserved.
//

import UIKit
var toDoList = [String]()  //defining an empty to-do list array


class FirstViewController: UIViewController,UITableViewDelegate {
    
    

    @IBOutlet weak var toDoListTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //if stored list exists, then
        if (NSUserDefaults.standardUserDefaults().objectForKey("toDoList") != nil) {
            toDoList = NSUserDefaults.standardUserDefaults().objectForKey("toDoList") as! [String]
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return toDoList.count
    }
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        cell.textLabel?.text = toDoList[indexPath.row]
        return cell
    }
    
    //method is called when the user tries to edit an item in the table
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        
        //swipe to the left on a cell is UITableViewCellEditingStyle.Delete
        if editingStyle == UITableViewCellEditingStyle.Delete {
            //indexPath.row to refer to the specific row
            toDoList.removeAtIndex(indexPath.row)
            
            //updating NSUserDefaults
            NSUserDefaults.standardUserDefaults().setObject(toDoList, forKey: "toDoList")
            
            //updating table
            toDoListTable.reloadData()
        }
    }
    
    
    //called everytime view is reloaded or reappeared
    override func viewDidAppear(animated: Bool) {
        print("View Loaded");
        toDoListTable.reloadData()
    }

}

