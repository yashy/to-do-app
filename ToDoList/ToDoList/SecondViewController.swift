//
//  SecondViewController.swift
//  ToDoList
//
//  Created by yash yadav on 3/17/16.
//  Copyright © 2016 yash yadav. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var item: UITextField!
    
    @IBAction func itemAction(sender: AnyObject){
        toDoList.append(item.text!)
        
        item.text = ""  //set it back so that user can add in more items
        
        //Update the value to store the data
        NSUserDefaults.standardUserDefaults().setObject(toDoList, forKey: "toDoList")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //disable keyboard when user taps outside the keyboard
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    //disable keyboard on pressing return
    func textFieldShouldReturn (textField: UITextField!) -> Bool{
        item.resignFirstResponder()
        return true
    }
    
}

